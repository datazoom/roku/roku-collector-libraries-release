Sub init()
    m.device = CreateObject("roDeviceInfo")
    m.global.observeField("input", "onInputReceived")
    m.eventConfig = invalid
    m.bufferFillVal = 0
    m.template = {}
    m.template.event = {}
    m.template.user_details = {}
    m.template.device = {}
    m.template.player ={}
    m.template.video ={}
    m.template.geo_location = {}
    m.template.network = {}
    m.template.custom = CreateObject("roAssociativeArray")
    m.template.event.attributes = {}
    m.template.ad = {}
    m.template.page = {}
    m.template.ops_metadata = {}
    m.oldposition=0
    m.playerPlay = false
    m.playerPlayRequest = false
    m.playerResume = false
    m.bufferStarted = false
    m.videoPaused = false
    m.playStateTimer = m.top.findNode("playStateTimer")
    m.playStateTimer.ObserveField("fire","playStateTimerFunction")
    m.quartileTimer = m.top.findNode("quartileTimer")
    m.quartileTimer.ObserveField("fire","quartileTimerFunction")
    m.contentFirstQuartile = true
    m.contentSecondQuartile = true
    m.contentThirdQuartile = true
    m.quartilePresence = false
    m.dataTimer = m.top.findNode("dataTimer")
    m.dataTimer.ObserveField("fire","dataTimerFunction")
    m.renditionName = ""
    m.renditionBitrate = 0
    m.renditionVideoBitrate = 0
    m.renditionAudioBitrate = 0
    m.streamBitrate = 0
    m.mute = false
    m.defaultMute = false
    m.playCounter = 0
    m.defaultRate = 1
    m.milestonePercent = 0
    m.playerHeight = 0
    m.playerWidth = 0
    m.renditionHeight = 0
    m.renditionWidth = 0
    m.streamBitrate = 0
    m.systemBitrate = 0
    m.pausePoint = 0
'    m.sessionStartTimestamp = 0
    m.numberOfErrors = 0
    m.bufferDuration = 0
    m.bufferStartTime = 0
    m.adBufferStartTime = 0
    m.stallStartTime = 0
    
    
    m.TSBUFFERSTART = 0
    m.TSSTALLSTART = 0
    m.TSADSTALLSTART = 0
    m.TSLASTHEARTBEAT = 0
    m.TSLASTMILESTONE = 0
    m.TSLASTADHEARTBEAT = 0
    m.TSLASTADMILESTONE = 0
    m.TSPAUSED = 0
    m.TSREQUESTED = 0
    m.TSSTARTED = 0
    m.TSADSTARTED = 0
    m.TSLOADED = 0
    m.TSRENDITIONCHANGE = 0
    m.playerAction = ""
    m.TSADIMPRESSION = 0
    m.TSADBREAKSTART = 0
    m.adSeekStartTime = 0
    m.seekStartTime = 0
    m.networkBandwidth = 0
    
    m.fired10 = false
    m.fired25 = false
    m.fired50 = false
    m.fired75 = false
    m.fired90 = false
    m.fired95 = false
    
    m.tempCustomMeta = {}
    
    m.adFired10 = false
    m.adFired25 = false
    m.adFired50 = false
    m.adFired75 = false
    m.adFired90 = false
    m.adFired95 = false
    m.lastAdMilestoneTime = 0
    m.lastAdHeartbeatTime = 0
    m.lastAdTime = 0
    
    m.videoType = "Content"
    m.engagementContentStart = 0
    m.engagementContendDuration = 0 
    m.oldAdTime = 0
    m.adTime = 0
    m.playContentDuration = 0
    m.playbackDuration = 0
    m.adUrl = ""
    m.adTitle = ""
    m.adServer = ""
    
    
    m.playbackStallCount = 0
    m.playbackStallCountAds = 0
    m.playbackStallCountContent = 0
    
    m.playbackStallDuration = 0
    m.playbackStallDurationAds = 0
    m.playbackStallDurationContent = 0
    m.tmpPlaybackTimer = 0
    m.pauseDuration = 0 
    m.firstFrameTriggered = false
    m.playbackDurationContent = 0
    m.heartCount = 1
    m.playbackCompleteTriggered = false
    m.playRequestTriggered = false
    m.playingDuration = 0
    m.eventCount = 0
End Sub

'Function to get player Type
function playerType()
    return "native"
end function

'Function to get deviceType
function deviceType()
    return "ott device"
end function

'Function containing Datapoint List
Function getLibConfig()
    if m.config = invalid
        m.config = {
          events: {
            EVENTPLAY: "Play",
            EVENTPAUSE: "Pause",
            EVENTBUFFERING: "Buffering",
            EVENTBUFFEREND: "Buffer_End",
            EVENTBUFFERSTART: "Buffer_Start",
            EVENTSTALLSTART: "Stall_Start",
            EVENTSTALLEND: "Stall_End",
            EVENTFIRSTFRAME: "Playback_Start",
            EVENTPLAYBACKCOMPLETE: "Playback_Complete",
            EVENTPLAYREQUEST: "Play_Request",
            EVENTPLAYING: "Playing",
            EVENTRESUME: "Resume",
            EVENTPLAYERREADY: "Player_Ready",
            EVENTCFQ: "Content_First_Quartile",
            EVENTCSQ: "Content_Second_Quartile",
            EVENTCTQ: "Content_Third_Quartile",
            EVENTMUTE: "Mute",
            EVENTUNMUTE: "Unmute",
            EVENTBITRATECHANGE: "Bitrate_Change",
            EVENTADREQUEST: "Ad_Request",
            EVENTADCOMPLETE: "Ad_Complete",
            EVENTADIMPRESSION: "Ad_Impression",
            EVENTDATAZOOMLOADED: "Datazoom_Loaded",
            EVENTCUSTOM: "Custom_Event",
            EVENTHEARTBEAT: "Heartbeat",
            EVENTCONTENTLOADED: "Content_Loaded",
            EVENTMILESTONE: "Milestone",
            EVENTRENDITIONCHANGE: "Rendition_Change",
            EVENTSEEKSTART: "Seek_Start",
            EVENTSEEKEND: "Seek_End",
            EVENTERROR: "Error",
            EVENTADCLICK:"Ad_Click",
            EVENTADLOADED: "Ad_Loaded",
            EVENTADPLAY: "Ad_Play",
            EVENTADERROR: "Ad_Error",
            EVENTADRENDITIONCHANGE: "Ad_Rendition_Change",
            EVENTADPAUSE: "Ad_Pause",
            EVENTADRESUME: "Ad_Resume",
            EVENTADBREAKSTART: "Ad_Break_Start",
            EVENTADBREAKEND: "Ad_Break_End",
            EVENTADCOMPLETE: "Ad_Complete",
            EVENTADREQUEST: "Ad_Request",
            
            METADURATION: "duration",
            METAIP: "client_ip",
            METACITY: "city",
            METACOUNTRYCODE: "countryCode",
            METAREGIONCODE: "regionCode",
            METAOS: "os",
            METADEVICETYPE: "deviceType",
            METADEVICEID: "device_id",
            METADEVICENAME: "deviceName",
            METADEVICEMFG: "deviceMfg",
            METAOSVERSION: "osVersion",
            METAASN: "asn",
            METAASNORG: "asnOrganization",
            METAISP: "isp",
            METACOUNTRY: "country",
            METAREGION: "region",
            METAZIP: "zipCode",
            
            METACONTROLS: "controls",
            METALOOP: "loop",
            METAREADYSTATE: "readyState",
            METASESSIONVIEWID: "SessionViewId",
            METAVIEWID: "viewId",
            METALONGITUDE: "longitude",
            METALATITUDE: "latitude",
            METADESCRIPTION: "description",
            METATITLE: "title",
            METAVIDEOTYPE: "videoType",
            METADEFAULTMUTED: "defaultMuted",
            METAISMUTED: "isMuted",
            METASOURCE: "source",
            METACUSTOM: "customMetadata",
            METADEFAULTPLAYBACKRATE: "defaultPlaybackRate",
            METAMILESTONEPERCENT: "milestonePercent",
            METAABSSHIFT: "absShift",
            METASEEKENDPOINT: "seekEndPoint",
            METASEEKSTARTPOINT: "seekStartPoint",
            METASESSIONSTARTTIMESTAMP: "sessionStartTimestamp",
            METAPLAYERHEIGHT: "playerHeight",
            METAPLAYERWIDTH: "playerWidth",
            METAFRAMERATE: "frameRate",
            METAADVERTISINGID: "advertisingId",
            METAERRORCODE: "errorCode",
            METAERRORMSG: "errorMsg",
            METAADPOSITION: "adPosition",
            METAUSERAGENT: "user_agent",
            METAPLAYERVERSION: "playerVersion",
            METADZSDKVERSION: "dzSdkVersion",
            METACONNECTIONTYPE: "connectionType",
            METAEVENTCOUNT: "eventCount",
            METASTREAMINGPROTOCOL: "streamingProtocol",
            METASTREAMINGTYPE: "streamingType",
            METATOTALSTARTUPDURATION: "totalStartupDuration",
            METAADSYSTEM: "adSystem",
            
            FLUXPLAYHEADUPDATE: "playheadPosition",
            FLUXBUFFERFILL: "bufferFill",
            FLUXPLAYERSTATE: "playerState",
            FLUXNUMBEROFVIDEOS: "numberOfVideos",
            FLUXRENDITIONBITRATE: "renditionBitrate",
            FLUXTSBUFFERSTART: "timeSinceBufferStart",
            FLUXTSSTALLSTART: "timeSinceStallStart",
            FLUXTSADSTALLSTART: "timeSinceAdStallStart",
            FLUXTSLASTHEARTBEAT: "timeSinceLastHeartbeat",
            FLUXTSLASTMILESTONE: "timeSinceLastMilestone",
            FLUXTSPAUSED: "timeSincePaused",
            FLUXTSREQUESTED: "timeSinceRequested",
            FLUXTSSTARTED: "timeSinceStarted",
            FLUXPLAYBACKDURATION: "playbackDuration",
            FLUXNUMBEROFADS: "numberOfAds",
            FLUXBITRATE: "bitrate",
            FLUXVIEWSTARTTIME: "viewStartTimestamp",
            FLUXRENDITIONHEIGHT: "renditionHeight",
            FLUXRENDITIONWIDTH: "renditionWidth",
            FLUXRENDITIONNAME: "renditionName",
            FLUXRENDITIONVIDEOBITRATE: "renditionVideoBitrate",
            FLUXRENDITIONAUDIOBITRATE: "renditionAudioBitrate",
            FLUXTSLASTRENDITIONCHANGE: "timeSinceLastRenditionChange",
            FLUXNUMBEROFERRORS: "numberOfErrors"
            FLUXTSLASTADMILESTONE: "timeSinceLastAdMilestone",
            FLUXTSLASTADHEARTBEAT: "timeSinceLastAdHeartbeat",
            FLUXBUFFERDURATION: "bufferDuration",
            FLUXENGAGEMENTDURATION: "engagementDuration",
            FLUXENGAGEMENTDURATIONCONTENT: "engagementDurationContent",
            FLUXPLAYBACKDURATIONCONTENT: "playbackDurationContent",
            FLUXPLAYBACKSTALLDURATIONCONTENT: "playbackStallDurationContent",
            FLUXPLAYBACKSTALLDURATIONADS: "playbackStallDurationAds",
            FLUXPLAYBACKSTALLDURATION: "playbackStallDuration",
            FLUXPLAYBACKSTALLCOUNTCONTENT: "playbackStallCountContent",
            FLUXPLAYBACKSTALLCOUNTADS: "playbackStallCountAds",
            FLUXPLAYBACKSTALLCOUNT: "playbackStallCount",
            FLUXNUMBEROFADSPLAYED: "numberOfAdsPlayed",
            FLUXENGAGEMENTDURATIONADS: "engagementDurationAds",
            FLUXPLAYBACKDURATIONADS: "playbackDurationAds",
            FLUXTSLASTAD: "timeSinceLastAd"
            FLUXTSADBREAKSTART: "timeSinceAdBreakStart",
            FLUXTSADREQUESTED: "timeSinceAdRequested",
            FLUXTSADSTARTED: "timeSinceAdStarted",
            FLUXTSADBUFFERSTART: "timeSinceAdBufferStart",
            FLUXBANDWIDTH: "bandwidth"
            FLUXTSADSEEKSTART: "timeSinceAdSeekStart",
            FLUXTSSEEKSTART: "timeSinceSeekStart"
          }
        }
    end if
    return m.config
end Function
'--------------------------- Custom event and meta functions ----------------------------
function generateDatazoomEvent(customEvent as String, customEventMeta = invalid)
'    configureEvents()
'    playerMetrics()
    if customEventMeta <> invalid
    m.tempCustomMeta.Append(m.template.custom)
    m.template.custom.append (customEventMeta)
    wsSend(getMessageTemplate("Custom_"+customEvent))
    m.template.custom = {}
    m.template.custom.Append(m.tempCustomMeta)
    end if
    wsSend(getMessageTemplate("Custom_"+customEvent))
End function  

Function setDatazoomMetadata(key = invalid, value = invalid, customMetadata = invalid)
    if type(key) = "roAssociativeArray"
    m.template.custom.append (key)
    else if key <> invalid and value <> invalid
    m.template.custom[key] = value
    end if
End Function
'----------------------------------------------------------------------------------------

'------------- Ad Events and Meta ------------------------------------------------------

function generateAdEvent(adData = invalid)
if adData <> invalid
    playerMetrics()
    If checkIfMetaConfigured(m.eventConfig.METAADSYSTEM) and adData.adserver <> invalid
         m.template.ad["adSystem"] = adData.adserver
    end if
if adData.rendersequence <> invalid then m.template.ad["adPosition"] = adData.rendersequence
If adData.type = "AdRequest"
          m.adBufferStartTime = getCurrentTimestampInMillis()
          m.TSADSTARTED = getCurrentTimestampInMillis()
          m.videoType = "Ad"
          
          if checkIfEventConfigured(m.eventConfig.EVENTADREQUEST)
          wsSend(getMessageTemplate(m.eventConfig.EVENTADREQUEST)) 
          end If
          
end if
If adData.type = "PodStart"
          m.TSADBREAKSTART = getCurrentTimestampInMillis()
          if checkIfEventConfigured(m.eventConfig.EVENTADBREAKSTART)
          wsSend(getMessageTemplate(m.eventConfig.EVENTADBREAKSTART)) 
          end If  
end if

If adData.type = "Impression" 'and checkIfEventConfigured(m.eventConfig.ADIMPRESSION)
           m.TSADIMPRESSION = getCurrentTimestampInMillis()
          if checkIfEventConfigured(m.eventConfig.EVENTADLOADED)
          wsSend(getMessageTemplate(m.eventConfig.EVENTADLOADED)) 
          end If
          if checkIfEventConfigured(m.eventConfig.EVENTADPLAY)
          wsSend(getMessageTemplate(m.eventConfig.EVENTADPLAY)) 
          end If
          if checkIfEventConfigured(m.eventConfig.EVENTADIMPRESSION)
          wsSend(getMessageTemplate(m.eventConfig.EVENTADIMPRESSION)) 
          end If
          m.videoType = "Ad"
          m.adUrl = adData.ad.streams[0].url
          m.adTitle = adData.Ad.adtitle
          m.template.ad["adId"] = adData.ad.adid

end if 

If adData.type = "Complete"
          if checkIfEventConfigured(m.eventConfig.EVENTADCOMPLETE)
          wsSend(getMessageTemplate(m.eventConfig.EVENTADCOMPLETE)) 
          end If
          if checkIfEventConfigured(m.eventConfig.EVENTADBREAKEND)
          wsSend(getMessageTemplate(m.eventConfig.EVENTADBREAKEND)) 
          end If
          m.numberOfAdsPlayed = m.numberOfAdsPlayed + 1
          m.videoType = "Content"
          m.engagementContentStart = getCurrentTimestampInMillis()
          m.lastAdTime = getCurrentTimestampInMillis()
          m.adFired10 = false
          m.adFired25 = false
          m.adFired50 = false
          m.adFired75 = false
          m.adFired90 = false
          m.adFired95 = false  
end if 

If adData.type = "Pause"
          if checkIfEventConfigured(m.eventConfig.EVENTADPAUSE)
          wsSend(getMessageTemplate("Ad_Pause")) 
          end If
end if

If adData.type = "Resume"
          if checkIfEventConfigured(m.eventConfig.EVENTADRESUME)
          wsSend(getMessageTemplate("Ad_Resume")) 
          end If
end if

If adData.errMsg <> invalid
    print "*****   Error message: " + adData.errMsg
    wsSend(getMessageTemplate("Ad_Error"))
end if

end if  

if adData.duration <> invalid and adData.time <> invalid and adData.time < adData.duration
    if adData.time / adData.duration > 0.1 and m.adFired10 = false 
    m.template.event.attributes["milestonePercent"] = 0.10
    wsSend(getMessageTemplate("Milestone"))
    m.adFired10 = true
    m.template.event.attributes = {}
    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
    end if
    if adData.time / adData.duration > 0.25 and m.adFired25 = false 
    m.template.event.attributes["milestonePercent"] = 0.25
    wsSend(getMessageTemplate("Milestone")) 
    m.adFired25 = true
    m.template.event.attributes = {}
    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
    end if
    if adData.time / adData.duration > 0.50 and m.adFired50 = false 
    m.template.event.attributes["milestonePercent"] = 0.50
    wsSend(getMessageTemplate("Milestone"))
    m.adFired50 = true
    m.template.event.attributes = {}
    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
    end if
    if adData.time / adData.duration > 0.75 and m.adFired75 = false
    m.template.event.attributes["milestonePercent"] = 0.75 
    wsSend(getMessageTemplate("Milestone"))  
    m.adFired75 = true
    m.template.event.attributes = {}
    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
    end if
    if adData.time / adData.duration > 0.85 and m.adFired90 = false 
    m.template.event.attributes["milestonePercent"] = 0.90
    wsSend(getMessageTemplate("Milestone"))
    m.adFired90 = true
    m.template.event.attributes = {}
    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
    end if
    if adData.time / adData.duration > 0.90 and m.adFired95 = false 
    m.template.event.attributes["milestonePercent"] = 0.95
    wsSend(getMessageTemplate("Milestone"))
    m.adFired95 = true
    m.template.event.attributes = {}
    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
    end if
end if

if m.top.events.interval <> invalid and adData <> invalid

if adData.time <> invalid and adData.time = 1
m.adTime = 1
m.playbackDuration = (m.playbackDuration + 1)

else if m.adTime > 0
m.adTime = m.adTime + 1
m.playbackDuration = (m.playbackDuration + 1)

end if

If (m.top.events.interval/1000) - (m.adTime - m.oldAdTime) = 0 
wsSend(getMessageTemplate("Heartbeat"))
m.oldAdTime = m.adTime
m.lastAdHeartbeatTime = getCurrentTimestampInMillis()
end if

end if

End function  


'---------------------------------------------------------------------------------------




'============== custom metadata and events Old implementation =======================
'Sub generateDatazoomEvent()
'    if m.top.generateDatazoomEvent
'    if m.top.customEventMeta <> invalid
'    m.tempCustomMeta.Append(m.template.custom)
'    m.template.custom.append (m.top.customEventMeta)
'    wsSend(getMessageTemplate("Custom_"+m.top.customEvent))
'    m.template.custom = {}
'    m.template.custom.Append(m.tempCustomMeta)
'    end if
'    wsSend(getMessageTemplate("Custom_"+m.top.customEvent))
'    End if
'End Sub  
'
'Sub setDatazoomMetadata()
'    if m.top.setDatazoomMetadata
'    if m.top.customMetadata <> invalid
'    m.template.custom.append (m.top.customMetadata)
'    else if m.top.customMetaKey <> invalid and m.top.customMetaValue <> invalid
'    m.template.custom[m.top.customMetaKey] = m.top.customMetaValue
'    end if
'    end if
'End Sub  
'=================================

' Configure maximum possible events needed to be collected from player.
Sub configureEvents() 
    m.player = m.top.playerInit.player
    playStateTimerFunction()
    m.playStateTimer.control = "start"
    m.player.observeField("position", "OnHeadPositionChange")
    m.player.observeField("bufferingStatus","OnBufferingStatusChange")
    m.player.observeField("state","OnVideoPlayerStateChange")
    m.player.observeField("timedMetaData", "timedMetaDataChanged")
    m.defaultMute = m.player.mute
    m.quartileTimer.control = "start"
    m.dataTimer.control = "start"
    m.defaultRate = 1
    m.numberOfErrors = 0
    m.displayMode = m.device.getDisplayMode()
    m.displaySize = m.device.getDisplaySize()
    m.playerHeight = m.displaySize.h
    m.playerWidth = m.displaySize.w
    
End Sub

Sub timedMetaDataChanged()
print "Position" m.player.Position
print "timedMetaData" m.player.timedMetaData
end Sub

' Checks if specific data points is available or not
function dataPointValidation()
    If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
        m.quartilePresence = true
    End If
    If checkIfEventConfigured(m.eventConfig.EVENTCSQ)
        m.quartilePresence = true
    End If
    If checkIfEventConfigured(m.eventConfig.EVENTCTQ)
        m.quartilePresence = true
    End If
end function

Function onInputReceived()
print "onInputReceived" 
End Function


' Player callback method on each seconds. Use to get the user current position in the playback. 
Sub OnHeadPositionChange()
    if m.quartilePresence
        resetQuartileTimerFunction()
    end if
    If m.top.events <> invalid
        if m.top.events.flux_data <> invalid
            fluxAvail = false
           For Each fluxType in m.top.events.flux_data
            fluxAvail=true
           end for
           ' print fluxCount
           if fluxAvail
                if((m.player.position-m.oldposition)<0)
                    m.oldposition=0
                end if
                
'                if ((m.player.position-m.oldposition)*1000) >= m.top.events.interval
                    m.oldposition=m.player.position
                        fluxMetricsData()
                        if m.syslog <> invalid
                        ? "MOMO BANDWIDTH = "; m.syslog
                        end if
                        if m.playbackDurationContent >= m.top.events.interval * m.heartCount
                        wsSend(getMessageTemplate("Heartbeat"))
                        m.TSLASTHEARTBEAT = getCurrentTimestampInMillis()
                        m.heartCount = m.heartCount + 1
'                end if
'                        wsSend(getMessageTemplate("Heartbeat"))    
                end if
            end if
        end if
    end if
    'Calls base to set session time
    callSetSessionTime()
End  Sub

function fluxMetricsData()
    m.template.event.metrics = {}
    If checkIfFluxConfigured(m.eventConfig.FLUXPLAYHEADUPDATE)
        playHead = 0
        if m.player <> invalid
            if m.player.position <> invalid
                playHead = m.player.position
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXPLAYHEADUPDATE] = playHead
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKDURATION)
        if m.player <> invalid
            if m.player.position <> invalid
                if m.playbackTimer <> invalid
                m.playingDuration = (m.playbackDuration * 1000) + (m.playbackTimer.TotalMilliseconds() - m.bufferDuration - m.pauseDuration)
              
                else
                m.playingDuration = (m.playbackDuration * 1000) '+ (playHead * 1000)
               
                end if
            end if
        end if
        if m.playingDuration > 0
        m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATION] = m.playingDuration
        else
        m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATION] = 0
        end if
        m.playingDuration = 0
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKDURATIONCONTENT)
        m.playbackDurationContent = 0
'        if m.player <> invalid
'            if m.player.position <> invalid
'                playbackDurationContent = m.player.position
'            end if
'        end if
        if m.playbackTimer <> invalid
                m.playbackDurationContent = m.playbackTimer.TotalMilliseconds() - m.bufferDuration - m.pauseDuration
                else
                
                end if  
        if m.playbackDurationContent > 0
        m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATIONCONTENT] = m.playbackDurationContent
        else
        m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATIONCONTENT] = 0
        end if
    end if
    
      if checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKDURATIONADS)
    if m.bufferDuration > 0
        m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATIONADS] = m.playbackDuration * 1000
    end if
    end if
    
    
    
    if checkIfFluxConfigured(m.eventConfig.FLUXBUFFERDURATION)
    if m.bufferDuration > 0
        m.template.event.metrics[m.eventConfig.FLUXBUFFERDURATION] = m.bufferDuration
    end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXBUFFERFILL)
        m.template.event.metrics[m.eventConfig.FLUXBUFFERFILL] = m.bufferFillVal
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXPLAYERSTATE)
        playerState = ""
        if m.player <> invalid
            if m.player.state <> invalid
                playerState = m.player.state
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXPLAYERSTATE] = playerState
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFVIDEOS)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFVIDEOS] = Val(getNoOfVideos())
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFERRORS)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFERRORS] = m.numberOfErrors
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFADS)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFADS] = 1
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFADSPLAYED) and m.numberOfAdsPlayed <> invalid
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFADSPLAYED] = m.numberOfAdsPlayed
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONBITRATE)
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONBITRATE] = m.renditionBitrate
    end if
    
     If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONVIDEOBITRATE)
        if m.player <> invalid
            if m.player.streamingSegment <> invalid
                if m.player.streamingSegment.segType = 2
                m.renditionVideoBitrate = m.player.streamingSegment.segBitrateBps
                end if
            else
            m.renditionVideoBitrate = m.renditionBitrate
           end if
            
        end if
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONVIDEOBITRATE] = m.renditionVideoBitrate
    end if
     If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONAUDIOBITRATE)
     if m.player <> invalid
            if m.player.streamingSegment <> invalid
                if m.player.streamingSegment.segType = 1
                m.renditionAudioBitrate = m.player.streamingSegment.segBitrateBps
                end if
                else
                m.renditionAudioBitrate = m.streamBitrate
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONAUDIOBITRATE] = m.renditionAudioBitrate
    end if
     If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONNAME)
        if m.player <> invalid
            if m.player.streamingSegment <> invalid
                m.renditionName = m.player.streamingSegment.segUrl
            end if
        end if
         m.template.event.metrics[m.eventConfig.FLUXRENDITIONNAME] = m.renditionName
    end if
     If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONWIDTH)
        if m.player <> invalid
            if m.player.downloadedSegment <> invalid
                if m.player.downloadedSegment.width > 0
                m.renditionWidth = m.player.downloadedSegment.width
                else
                m.renditionWidth = m.playerWidth
                end if
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONWIDTH] = m.renditionWidth
    end if
     If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONHEIGHT)
     if m.player <> invalid
            if m.player.downloadedSegment <> invalid
                if m.player.downloadedSegment.height > 0
                m.renditionHeight = m.player.downloadedSegment.height
                else
                m.renditionHeight = m.playerHeight
                end if
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONHEIGHT] = m.renditionHeight
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXBITRATE)
    if m.player <> invalid
            if m.player.state <> invalid
                bitrate = m.renditionBitrate
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXBITRATE] = m.renditionBitrate
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXBANDWIDTH)
        m.template.event.metrics[m.eventConfig.FLUXBANDWIDTH] = m.networkBandwidth
    end if

    ' Time Since flux metrics    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSBUFFERSTART)
        if m.TSBUFFERSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXTSBUFFERSTART] = m.TSBUFFERSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXTSBUFFERSTART] = getCurrentTimestampInMillis() - m.bufferStartTime
        end if
    end if
        If checkIfFluxConfigured(m.eventConfig.FLUXTSADBUFFERSTART)
        ? "MOMO AD BUFFER START DETECTED!!!!!!!!!!!!!!!!"
        if m.adBufferStartTime = 0
            ? "MOMO AD BUFFER START TIME = 0"
            m.template.event.metrics[m.eventConfig.FLUXTSADBUFFERSTART] = m.adBufferStartTime
        else
            ? "MOMO AD BUFFER START TIME >0 "
            m.template.event.metrics[m.eventConfig.FLUXTSADBUFFERSTART] = getCurrentTimestampInMillis() - m.adBufferStartTime
        end if
        
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSSTALLSTART)
        if m.TSSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXTSSTALLSTART] = m.TSSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXTSSTALLSTART] = getCurrentTimestampInMillis() - m.TSSTALLSTART
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSADSTALLSTART)
        if m.TSADSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXTSADSTALLSTART] = m.TSADSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXTSADSTALLSTART] = getCurrentTimestampInMillis() - m.TSADSTALLSTART
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSLASTHEARTBEAT)
        if m.TSLASTHEARTBEAT = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTHEARTBEAT] = m.TSLASTHEARTBEAT
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTHEARTBEAT] = getCurrentTimestampInMillis() - m.TSLASTHEARTBEAT
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSLASTADHEARTBEAT)
        if m.lastAdHeartbeatTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTADHEARTBEAT] = m.lastAdHeartbeatTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTADHEARTBEAT] = getCurrentTimestampInMillis() - m.lastAdHeartbeatTime
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSLASTMILESTONE)
        if m.TSLASTMILESTONE = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTMILESTONE] = m.TSLASTMILESTONE
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTMILESTONE] = getCurrentTimestampInMillis() - m.TSLASTMILESTONE
        end if
    end if
        If checkIfFluxConfigured(m.eventConfig.FLUXTSLASTADMILESTONE)
        
        if m.lastAdMilestoneTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTADMILESTONE] = m.lastAdMilestoneTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTADMILESTONE] = getCurrentTimestampInMillis() - m.lastAdMilestoneTime
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSLASTAD)
        if m.lastAdTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTAD] = m.lastAdTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTAD] = getCurrentTimestampInMillis() - m.lastAdTime
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSPAUSED)
        if m.TSPAUSED = 0
            m.template.event.metrics[m.eventConfig.FLUXTSPAUSED] = m.TSPAUSED
        else
            m.template.event.metrics[m.eventConfig.FLUXTSPAUSED] = getCurrentTimestampInMillis() - m.TSPAUSED
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSREQUESTED)
        if m.TSREQUESTED = 0
            m.template.event.metrics[m.eventConfig.FLUXTSREQUESTED] = m.TSREQUESTED
        else
            m.template.event.metrics[m.eventConfig.FLUXTSREQUESTED] = getCurrentTimestampInMillis() - m.TSREQUESTED
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXVIEWSTARTTIME)
        if m.TSREQUESTED = 0
            m.template.event.metrics[m.eventConfig.FLUXVIEWSTARTTIME] = m.TSREQUESTED
        else
            m.template.event.metrics[m.eventConfig.FLUXVIEWSTARTTIME] = getCurrentTimestampInMillis() - m.TSREQUESTED
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSSTARTED)
        if m.TSSTARTED = 0
            m.template.event.metrics[m.eventConfig.FLUXTSSTARTED] = m.TSSTARTED
        else
            m.template.event.metrics[m.eventConfig.FLUXTSSTARTED] = getCurrentTimestampInMillis() - m.TSSTARTED
        end if
    end if
    
     If checkIfFluxConfigured(m.eventConfig.FLUXENGAGEMENTDURATIONCONTENT)
        if m.TSSTARTED = 0
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATIONCONTENT] = m.TSSTARTED
        else
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATIONCONTENT] = getCurrentTimestampInMillis() - m.TSSTARTED
        end if
    end if
    
     If checkIfFluxConfigured(m.eventConfig.FLUXENGAGEMENTDURATIONADS)
        if m.TSADSTARTED = 0
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATIONADS] = m.TSADSTARTED
        else
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATIONADS] = getCurrentTimestampInMillis() - m.TSADSTARTED
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSADREQUESTED)
        if m.TSADSTARTED = 0
            m.template.event.metrics[m.eventConfig.FLUXTSADREQUESTED] = m.TSADSTARTED
        else
            m.template.event.metrics[m.eventConfig.FLUXTSADREQUESTED] = getCurrentTimestampInMillis() - m.TSADSTARTED
        end if
    end if
    
    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSADBREAKSTART)
        if m.TSADBREAKSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXTSADBREAKSTART] = m.TSADBREAKSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXTSADBREAKSTART] = getCurrentTimestampInMillis() - m.TSADBREAKSTART
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSADSTARTED)
        if m.TSADIMPRESSION = 0
            m.template.event.metrics[m.eventConfig.FLUXTSADSTARTED] = m.TSADIMPRESSION
        else
            m.template.event.metrics[m.eventConfig.FLUXTSADSTARTED] = getCurrentTimestampInMillis() - m.TSADIMPRESSION
        end if
    end if
    
         If checkIfFluxConfigured(m.eventConfig.FLUXENGAGEMENTDURATION)
        if m.TSLOADED = 0
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATION] = m.TSLOADED
        else
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATION] = getCurrentTimestampInMillis() - m.TSLOADED
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSSEEKSTART)
        if m.adSeekStartTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSADSEEKSTART] = m.adSeekStartTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSADSEEKSTART] = getCurrentTimestampInMillis() - m.adSeekStartTime
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSSEEKSTART)
        if m.seekStartTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSSEEKSTART] = m.seekStartTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSSEEKSTART] = getCurrentTimestampInMillis() - m.seekStartTime
        end if
    end if
    
    
'    If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKDURATIONCONTENT)
'            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATIONCONTENT] = 
'    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSLASTRENDITIONCHANGE)
        if m.TSRENDITIONCHANGE = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTRENDITIONCHANGE] = m.TSRENDITIONCHANGE
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTRENDITIONCHANGE] = getCurrentTimestampInMillis() - m.TSRENDITIONCHANGE
        end if
    end if
    
        If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLCOUNT)
        if m.TSSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNT] = m.TSSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNT] = m.playbackStallCountAds + m.playbackStallCountContent
        end if
    end if
    
     If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLCOUNTCONTENT)
        if m.TSSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNTCONTENT] = m.TSSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNTCONTENT] = m.playbackStallCountContent
        end if
    end if
    
     If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLCOUNTADS)
        if m.TSADSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNTADS] = m.TSADSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNTADS] = m.playbackStallCountAds
        end if
    end if
    
     If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLDURATION)
        if m.TSSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATION] = m.TSSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATION] = m.playbackStallDurationContent + m.playbackStallDurationAds
        end if
    end if
    
         If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLDURATIONCONTENT)
        if m.TSSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATIONCONTENT] = m.TSSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATIONCONTENT] = m.playbackStallDurationContent
        end if
    end if
    
         If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLDURATIONADS)
        if m.TSADSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATIONADS] = m.TSADSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATIONADS] = m.playbackStallDurationAds
        end if
    end if
    
    
end function

' Sets playheadPosition
function playerMetrics()
    m.template.event.metrics = {}
    'Change this variable value to set whether fluxdata values should be sent with events or not
    callFlexMetrics = true
    if callFlexMetrics
        fluxMetricsData()
    else
        playHead = 0
        If checkIfFluxConfigured(m.eventConfig.FLUXPLAYHEADUPDATE)
            if m.player <> invalid
                if m.player.position <> invalid
                   playHead = m.player.position
                end if
            end if
        end If
        m.template.event.metrics["playheadPosition"] = playHead
    end if
end function

' Player callback method for buffer status changes.
Sub OnBufferingStatusChange()
    if m.player.bufferingStatus<>invalid
        m.bufferFillVal = m.player.bufferingStatus.percentage
        if(m.bufferFillVal >= 95)
        if m.bufferStarted
            m.bufferStarted = false
            playerMetrics()
            If checkIfEventConfigured(m.eventConfig.EVENTBUFFEREND)
                wsSend(getMessageTemplate(m.eventConfig.EVENTBUFFEREND))
                 m.bufferEnd = true
            End If
            m.TSBUFFERSTART = getCurrentTimestampInMillis() - m.bufferStartTime
            m.bufferDuration = m.bufferDuration +  m.TSBUFFERSTART
            If checkIfEventConfigured(m.eventConfig.EVENTSTALLEND)
                wsSend(getMessageTemplate(m.eventConfig.EVENTSTALLEND))
                if m.videoType = "Content"

                m.playbackStallCountContent = m.playbackStallCountContent + 1
                m.playbackStallDurationContent = m.playbackStallDurationContent + (getCurrentTimestampInMillis() - m.bufferStartTime)
                end if
                
                if m.videoType = "Ad"
                m.playbackStallCountAds = m.playbackStallCountAds + 1
                m.playbackStallDurationAds = m.playbackStallDurationAds + (getCurrentTimestampInMillis() - m.bufferStartTime)
                end if   
            End If
          end if
        end if
   end if
End Sub

' Function to set resume flag
function setResume()
    m.playerResume = true
end function

' Player state change callback method.
sub OnVideoPlayerStateChange()
    playerMetrics()
    if m.player.state = "playing"
        m.resumePoint = m.player.position
        m.playCounter = m.playCounter + 1
        if m.playCounter >1
            setResume()
        end if
        m.videoPaused = false
        if m.playerResume
            m.resumePoint = m.player.position
            if  Abs(m.pausePoint - m.resumePoint) > 1
            m.pauseTimer = CreateObject("roTimespan")
             m.pauseTimer.Mark()
        If checkIfEventConfigured(m.eventConfig.EVENTSEEKSTART)
           if m.videoType = "Ad"
           m.adSeekStartTime = getCurrentTimestampInMillis()
           wsSend(getMessageTemplate(m.eventConfig.EVENTADSEEKSTART))
           end if
        End If        
        If checkIfEventConfigured(m.eventConfig.EVENTSEEKSTART)
           m.seekStartTime = getCurrentTimestampInMillis()
           wsSend(getMessageTemplate(m.eventConfig.EVENTSEEKSTART))
        End If  

         If checkIfEventConfigured(m.eventConfig.EVENTSEEKEND)
'                ? "PAUSE POINT:"; m.pausePoint
                m.template.event.attributes[m.eventConfig.METASEEKSTARTPOINT] = m.pausePoint * 1000
                m.template.event.attributes[m.eventConfig.METASEEKENDPOINT] = m.player.position * 1000
                wsSend(getMessageTemplate(m.eventConfig.EVENTSEEKEND))
                m.template.event.attributes = {}
            End If   
        
'        print ("SEEK EVENT")
'        print "PAUSE TIME"; m.pausePoint; "STARTED:"; m.resumePoint;
        end if
'        if m.pausePoint > m.resumePoint 
'         If checkIfEventConfigured(m.eventConfig.EVENTSEEKEND)
'                m.template.event.attributes[m.eventConfig.METASEEKSTART] = m.pausePoint
'                m.template.event.attributes[m.eventConfig.METASEEKSTART] = m.startPoint
'                wsSend(getMessageTemplate(m.eventConfig.EVENTSEEKEND))
'                m.template.event.attributes = {}
'            End If
'        end if
            if m.pauseTimer <> invalid
            m.pausedTime = m.pauseTimer.TotalMilliseconds()
            m.pauseDuration = m.pauseDuration + m.pausedTime
            m.pauseTimer = invalid
            end if
            playerMetrics()
            If checkIfEventConfigured(m.eventConfig.EVENTRESUME)
                wsSend(getMessageTemplate(m.eventConfig.EVENTRESUME))
            End If 
        else
            setResume()
            If checkIfEventConfigured(m.eventConfig.EVENTPLAY)
                wsSend(getMessageTemplate(m.eventConfig.EVENTPLAY))
            End If
            If checkIfEventConfigured(m.eventConfig.EVENTPLAYREQUEST)
            wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYREQUEST)) 
            m.playRequestTriggered = true                        
            End If
            If checkIfEventConfigured(m.eventConfig.EVENTFIRSTFRAME)
                startupDuration = m.startupTimer.TotalMilliseconds()
                m.template.event.attributes[m.eventConfig.METATOTALSTARTUPDURATION] = startupDuration
                wsSend(getMessageTemplate(m.eventConfig.EVENTFIRSTFRAME))
                m.firstFrameTriggered = true
                m.template.event.attributes = {}
            End If
            m.heartCount = 1
            m.TSSTARTED = getCurrentTimestampInMillis()
        end if
        If checkIfEventConfigured(m.eventConfig.EVENTPLAYREQUEST) and m.playRequestTriggered = false
            wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYREQUEST)) 
            m.playRequestTriggered = true                      
            End If
        If checkIfEventConfigured(m.eventConfig.EVENTFIRSTFRAME) and m.firstFrameTriggered = false
        wsSend(getMessageTemplate(m.eventConfig.EVENTFIRSTFRAME))
           m.heartCount = 1
        m.firstFrameTriggered = true 
        End If
        If checkIfEventConfigured(m.eventConfig.EVENTPLAYING)
            wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYING))
        End If
        
     else if m.player.state = "paused"
        m.videoPaused = true
        m.pausePoint = m.player.position
        m.TSPAUSED = getCurrentTimestampInMillis()
        m.pauseTimer = CreateObject("roTimespan")
        m.pauseTimer.Mark()
        If checkIfEventConfigured(m.eventConfig.EVENTPAUSE)
            wsSend(getMessageTemplate(m.eventConfig.EVENTPAUSE))
        End If

     else if m.player.state = "buffering"
        m.bufferStarted = true
        m.bufferStartTime = getCurrentTimestampInMillis()
        if m.videoType = "Ad"
        m.adBufferStartTime = getCurrentTimestampInMillis()
        end if
        m.bufferEnd = false

        if m.player.position > 1

        m.TSSTALLSTART = getCurrentTimestampInMillis()
        end if
        If checkIfEventConfigured(m.eventConfig.EVENTBUFFERING)
          wsSend(getMessageTemplate(m.eventConfig.EVENTBUFFERING))
        End If
        If checkIfEventConfigured(m.eventConfig.EVENTBUFFERSTART)
          wsSend(getMessageTemplate(m.eventConfig.EVENTBUFFERSTART))
        End If
        If checkIfEventConfigured(m.eventConfig.EVENTSTALLSTART)
          wsSend(getMessageTemplate(m.eventConfig.EVENTSTALLSTART))
        End If
     else if m.player.state = "error"
        If checkIfEventConfigured(m.eventConfig.EVENTERROR)
          m.template.event.attributes[m.eventConfig.METAERRORCODE] = m.player.errorCode.ToStr()
          m.template.event.attributes[m.eventConfig.METAERRORMSG] = m.player.errorMsg
          wsSend(getMessageTemplate(m.eventConfig.EVENTERROR)) 
          m.numberOfErrors = m.numberOfErrors + 1   
          m.template.event.attributes = {}  
        end if  
    else if m.player.state = "stopped"
         m.firstFrameTriggered = false
         m.playRequestTriggered = false
         m.template.custom = {}
         m.playingDuration = 0
         m.playbackDurationContent = 0
         m.TSLASTADHEARTBEAT = 0
        
    else if m.player.state = "finished"
    If checkIfEventConfigured(m.eventConfig.EVENTPLAYBACKCOMPLETE) and m.playbackCompleteTriggered = false
         wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBACKCOMPLETE))
         m.playbackCompleteTriggered = true
         m.template.custom = {}
    End If
    
    m.firstFrameTriggered = false
    end if    
end Sub

' Function to reset quartile flags
sub resetQuartileTimerFunction()
    if m.player <> invalid
        if m.player.position <> invalid and m.player.duration <> invalid
            playerPosition = m.player.position
            playerDuration = m.player.duration
            if playerDuration > 0 and playerPosition > 0
                if playerPosition < (playerDuration * 0.25)
                    m.contentThirdQuartile = true
                    m.contentSecondQuartile = true
                    m.contentFirstQuartile = true
                else if playerPosition < (playerDuration * 0.50)
                    m.contentThirdQuartile = true
                    m.contentSecondQuartile = true
                else if playerPosition < (playerDuration * 0.75)
                    m.contentThirdQuartile = true
                end if
            end if
        end if
    end if
end sub

' Runs at specific interval to check is quartile position is reached
sub quartileTimerFunction()
    videoRunning = false
    if m.player <> invalid
        if m.player.position <> invalid
            if m.player.position <> m.oldposition
                videoRunning = true
            end if
        end if
    end if
    if videoRunning
        if m.player <> invalid
            if m.player.position <> invalid and m.player.duration <> invalid
            
                playerPosition = m.player.position
                playerDuration = m.player.duration
                m.template.event.attributes = {}
                if playerDuration > 0 and playerPosition > 0
                        if playerPosition >= (playerDuration * 0.95) and m.fired95 = false
                            
                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
                                If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.95
                                end if
                                m.milestonePercent = 0.95
                                playerMetrics()
                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
                                m.fired95 = true
                                m.template.event.attributes = {}
                            End If
'                        end if
                        else if playerPosition >= (playerDuration * 0.9) and m.fired90 = false
                        if playerPosition < ((playerDuration * 0.9)+2) and m.fired90 = false
                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
                            If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.9
                                end if
                                m.milestonePercent = 0.9
                                playerMetrics()
                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
                                m.fired90 = true
                                m.template.event.attributes = {}
                            End If
                        end if
                    else if playerPosition >= (playerDuration * 0.75)  and m.fired75 = false
                        if playerPosition < ((playerDuration * 0.75)+1) and m.fired75 = false
                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
                            If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.75
                                end if
                                m.milestonePercent = 0.75
                                playerMetrics()
                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
                                m.fired75 = true
                                m.template.event.attributes = {}
                            End If
                        end if
                    else if playerPosition >= (playerDuration * 0.50) and m.fired50 = false
                        if playerPosition < ((playerDuration * 0.50)+1) and m.fired50 = false
                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
                            If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.50
                                end if
                                m.milestonePercent = 0.50
                                playerMetrics()
                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
                                m.fired50 = true
                                m.template.event.attributes = {}
                            End If
                        end if
                        else if playerPosition >= (playerDuration * 0.25) and m.fired25 = false
                        if playerPosition < ((playerDuration * 0.25)+1) and m.fired25 = false
                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
                            If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.25
                            end if
                                m.milestonePercent = 0.25
                                playerMetrics()
                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
                                m.fired25 = true
                                m.template.event.attributes = {}
                            End If
                        end if
                    else if playerPosition >= (playerDuration * 0.11) and m.fired10 = false
                         
                        if playerPosition <= ((playerDuration * 0.11)+1) and m.fired10 = false
                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
                            
                            If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.10
                            end if
                                m.milestonePercent = 0.10
                                playerMetrics()
                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
                                m.fired10 = true
                                m.template.event.attributes = {}
                            End If
                        end if
                    end if
                end if
            end if
        end if
    end if
end sub

' Runs at specific interval to update the data
sub dataTimerFunction()
    videoRunning = false
    if m.player <> invalid
        'is video running?
        if m.player.position <> invalid
            if m.player.position <> m.oldposition
                videoRunning = true
            end if
        end if
        'is video Muted
        if videoRunning
           videoMute= m.player.mute
           if m.mute <> videoMute
            m.mute = videoMute
            if m.mute
                If checkIfEventConfigured(m.eventConfig.EVENTMUTE)
                  playerMetrics()
                  wsSend(getMessageTemplate(m.eventConfig.EVENTMUTE))
                End If
            else
                If checkIfEventConfigured(m.eventConfig.EVENTUNMUTE)
                  playerMetrics()
                  wsSend(getMessageTemplate(m.eventConfig.EVENTUNMUTE))
                End If
            end if
        end if
        
       'Collect stream metadata and info
       
       'timedMetadata
       m.player.timedMetaDataSelectionKeys = ["*"]
       if m.player.timedMetaData <> invalid
            ba = CreateObject("roByteArray")
            ba.FromHExString(m.player.timedMetaData.PRIV)
            strg = ba.ToAsciiString()
        end if
       
       'stream info
       if m.player.streamInfo <> invalid 
       m.networkBandwidth = m.player.streamInfo.measuredBitrate
       end if 
       
       ' downloaded segment
       if m.player.downloadedSegment <> invalid
       end if
       
       'streaming segment
       if m.player.streamingSegment <> invalid 
           if m.player.streamingSegment.segBitrateBps <> invalid or m.player.streamingSegment.segBitrateBps <> 0 
           m.renditionBitrate = m.player.streamingSegment.segBitrateBps
           else if m.player.streamBitrate <> invalid or m.player.streamBitrate <> 0 
           m.renditionBitrate = m.player.streamBitrate
           else if m.player.streamInfo.streamBitrate <> invalid or m.player.streamInfo.streamBitrate <> 0 
           m.streamBitrate = m.player.streamInfo.streamBitrate
'           m.networkBandwidth = m.player.streamInfo.measuredBitrate / 1000
       end if

       end if
       if m.player.manifestData <> invalid
       end if
        if m.player.streamingSegment <> invalid
            if m.player.streamingSegment.segBitrateBps <> invalid
                 streamB = m.player.streamingSegment.segBitrateBps
                
                 m.renditionBitrate  = streamB
'                 m.networkBandwidth = m.renditionBitrate / 1000
                 if m.streamBitrate <> streamB
                 m.streamBitrate = streamB
                 playerMetrics()
                   if streamB < m.streamBitrate
                   m.template.event.attributes[m.eventConfig.METAABSSHIFT] = "up"
                   end if
                   if streamB > m.streamBitrate
                   m.template.event.attributes[m.eventConfig.METAABSSHIFT] = "down"
                   end if
                 m.streamBitrate = streamB
                 If checkIfEventConfigured(m.eventConfig.EVENTBITRATECHANGE)
                 playerMetrics()
                 wsSend(getMessageTemplate(m.eventConfig.EVENTBITRATECHANGE))
                 end if
                 If checkIfEventConfigured(m.eventConfig.EVENTRENDITIONCHANGE)
                 playerMetrics()
                 wsSend(getMessageTemplate(m.eventConfig.EVENTRENDITIONCHANGE))
                 m.TSRENDITIONCHANGE = getCurrentTimestampInMillis()
                 m.template.event.attributes = {}
                 end if
                 end if
            end if
        end if
    end if
    end if
end sub

' Checks when player is ready to play
function playStateTimerFunction()

    if m.player <> invalid
        if m.player.control <> invalid
            if m.player.control <> m.playerAction
                m.playerAction = m.player.control
                if LCase(m.playerAction) = "play"
                    callToSetNoOfVideos()
                    playerMetrics()                    
                    m.TSREQUESTED = getCurrentTimestampInMillis()
                    m.TSLASTADHEARTBEAT = 0
                    m.template.event.metrics[m.eventConfig.FLUXTSLASTHEARTBEAT] = 0
                    m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATIONCONTENT] = 0
                    m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATION] = 0
                    If checkIfEventConfigured(m.eventConfig.EVENTPLAYERREADY)
                     wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYERREADY))
                    End If
                    If checkIfEventConfigured(m.eventConfig.EVENTCONTENTLOADED)
                     wsSend(getMessageTemplate(m.eventConfig.EVENTCONTENTLOADED))
                    End If
'                    If checkIfEventConfigured(m.eventConfig.EVENTPLAYREQUEST)
'                        wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYREQUEST))       
'                        m.playRequestTriggered = true                 
'                    End If
                    m.playbackCompleteTriggered = false
                    m.startupTimer = CreateObject("roTimespan")
                    m.playbackTimer = CreateObject("roTimespan")
                end if
            end if
        end if
    end if
end function

'----------------------- Collector Starts --------------------------------

' Method to get the message template
Function getMessageTemplate(event as String) as object
    m.eventCount = m.eventCount + 1
    m.template.page[m.eventConfig.METAEVENTCOUNT] = m.eventCount
    If checkIfMetaConfigured(m.eventConfig.METASESSIONVIEWID)
         getContentUrl()
    end if
    
    If checkIfMetaConfigured(m.eventConfig.METAVIEWID)
         getContentUrl()
    end if
     m.template.customer_code = m.responseBody.customer_code
     m.template.connector_list= m.responseBody.connector_list
     m.template.configuration_id = m.responseBody.configuration_id
     m.template.event_id = m.device.GetRandomUUID()
     If checkIfMetaConfigured(m.eventConfig.METAUSERAGENT)
     m.template.user_details["user_agent"] = m.device.GetModelDisplayName()+"-"+m.device.GetModel()+"-"+m.device.GetVersion()+"-"+m.device.GetChannelClientId()
     end if
     If checkIfMetaConfigured(m.eventConfig.METADZSDKVERSION)
     m.template.page["dzSdkVersion"]= "1.65"
     end if
     m.template.event.type = event
     m.template.event.timestamp = getCurrentTimestampInMillis()
'     m.template.ops_metadata["client_ts_millis"] = m.template.event.timestamp
     'm.template.device.id = getUniqueDeviceId()
     
     m.template.user_details.is_anonymous = false
     m.template.user_details.session_id = getSessionData()
'     m.template.user_details.sessionStartTimestamp = m.sessionIdTimeKey
     m.template.player["playerName"] = "Roku"
     If checkIfMetaConfigured(m.eventConfig.METAPLAYERVERSION)
     m.template.player["playerVersion"] = getVersion()
     end if
     If checkIfMetaConfigured(m.eventConfig.METACONNECTIONTYPE)
     m.template.network["connectionType"] = getConnectionType()
     end if
     getMetaData()
     
     return m.template
End Function

'Function to get the metadata
function getMetaData()
    if m.player <> invalid and m.player.content <> invalid
    If checkIfMetaConfigured(m.eventConfig.METADURATION)
        playerDuration = 0
        if m.player <> invalid
            if m.player.duration <> invalid
                playerDuration = m.player.duration
            end if
        end if
        m.template.video[m.eventConfig.METADURATION] = playerDuration
    end if

    If checkIfMetaConfigured(m.eventConfig.METAIP)
        m.template.user_details[m.eventConfig.METAIP] = getIpAddress()
    end if
    If checkIfMetaConfigured(m.eventConfig.METACITY)
        m.template.geo_location[m.eventConfig.METACITY] = getCity()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAZIP)
        m.template.geo_location[m.eventConfig.METAZIP] = getZip()
    end if
    If checkIfMetaConfigured(m.eventConfig.METACOUNTRYCODE)
        m.template.geo_location[m.eventConfig.METACOUNTRYCODE] = getCountryCode()
    end if
    If checkIfMetaConfigured(m.eventConfig.METACOUNTRY)
        m.template.geo_location[m.eventConfig.METACOUNTRY] = getCountry()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAREGIONCODE)
        m.template.geo_location[m.eventConfig.METAREGIONCODE] = getRegionCode()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAREGION)
        m.template.geo_location[m.eventConfig.METAREGION] = getRegion()
    end if
        If checkIfMetaConfigured(m.eventConfig.METAOS)
        m.template.device[m.eventConfig.METAOS] = getOS()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEVICETYPE)
        m.template.device[m.eventConfig.METADEVICETYPE] = getDeviceType()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEVICEID)
        m.template.device[m.eventConfig.METADEVICEID] = getUniqueDeviceId()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAADVERTISINGID)
        adId = getAdId()
        m.template.device[m.eventConfig.METAADVERTISINGID] = getAdId()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEVICENAME)
        m.template.device[m.eventConfig.METADEVICENAME] = getModelDisplayName()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEVICEMFG)
        m.template.device[m.eventConfig.METADEVICEMFG] = "Roku, Inc."
    end if
    If checkIfMetaConfigured(m.eventConfig.METAVIDEOTYPE)
        m.template.video[m.eventConfig.METAVIDEOTYPE] = m.videoType
    end if
    If checkIfMetaConfigured(m.eventConfig.METAOSVERSION)
        m.template.device[m.eventConfig.METAOSVERSION] = getOSVersion()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAASN)
        m.template.network[m.eventConfig.METAASN] = getasn()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAASNORG)
        m.template.network[m.eventConfig.METAASNORG] = getasnOrg()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAISP)
        m.template.network[m.eventConfig.METAISP] = getISP()
    end if
    If checkIfMetaConfigured(m.eventConfig.METACONTROLS)
         metacontrol = false
        if m.player <> invalid
            if m.player.control <> invalid
                if m.player.control <> ""
                    metacontrol = true
                end if
            end if
        end if
        m.template.player[m.eventConfig.METACONTROLS] = metacontrol
    end if
    If checkIfMetaConfigured(m.eventConfig.METALOOP)
        metaloop = false
        if m.player <> invalid
            if m.player.loop <> invalid
                if m.player.loop
                    metaloop = true
                end if
            end if
        end if
        m.template.player[m.eventConfig.METALOOP] = metaloop
    end if
    If checkIfMetaConfigured(m.eventConfig.METAREADYSTATE)
        m.template.player[m.eventConfig.METAREADYSTATE] = getPlayerReadyState()
    end if
    If checkIfMetaConfigured(m.eventConfig.METASESSIONVIEWID)
        m.template.user_details[m.eventConfig.METASESSIONVIEWID] = getSessionViewId()
    end if
'    If checkIfMetaConfigured(m.eventConfig.METASESSIONSTARTTIMESTAMP)
'        m.template.user_details[m.eventConfig.METASESSIONSTARTTIMESTAMP] = m.sessionStartTimestamp
'    end if
    If checkIfMetaConfigured(m.eventConfig.METAVIEWID)
        m.template.user_details[m.eventConfig.METAVIEWID] = getSessionViewId()
    end if
    
    If checkIfMetaConfigured(m.eventConfig.METALONGITUDE)
        m.template.geo_location[m.eventConfig.METALONGITUDE] = getLongitude()
    end if
    If checkIfMetaConfigured(m.eventConfig.METALATITUDE)
        m.template.geo_location[m.eventConfig.METALATITUDE] = getLatitude()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADESCRIPTION)
        metadescription = ""
        if m.player <> invalid
            if m.player.content.description <> invalid
               metadescription = m.player.content.description
            end if
        end if
        m.template.video[m.eventConfig.METADESCRIPTION] = metadescription
    end if
    If checkIfMetaConfigured(m.eventConfig.METATITLE)
        metatitle = ""
        if m.videoType = "Ad" and m.adTitle <> invalid
        metatitle = m.adTitle
        end if
        if m.player <> invalid and m.videoType = "Content"
            if m.player.content.title <> invalid
               metatitle = m.player.content.title
            end if
            if m.player.content.url <> invalid
               metaurl = m.player.content.url
            end if
        end if
        m.template.video[m.eventConfig.METATITLE] = metatitle
    end if
        If checkIfMetaConfigured(m.eventConfig.METASOURCE)
        metatitle = ""
        if m.videoType = "Ad" and m.adUrl <> invalid
        metaurl = m.adUrl
        end if
        if m.player <> invalid and m.videoType = "Content"
            if m.player.content.url <> invalid
               metaurl = m.player.content.url
            end if
        end if
        m.template.video[m.eventConfig.METASOURCE] = metaurl
    end if
        If checkIfMetaConfigured(m.eventConfig.METASTREAMINGPROTOCOL)
        m.template.player[m.eventConfig.METASTREAMINGPROTOCOL] = m.player.videoFormat
    end if
    If checkIfMetaConfigured(m.eventConfig.METASTREAMINGTYPE)
        if m.player.content.Title = "Live Stream" and m.videoType <> "Ad"
        m.template.player[m.eventConfig.METASTREAMINGTYPE] = "Live"
        else 
        m.template.player[m.eventConfig.METASTREAMINGTYPE] = "VOD"
        end if
    end if
    If checkIfMetaConfigured(m.eventConfig.METAISMUTED)
        m.template.player[m.eventConfig.METAISMUTED] = m.mute
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEFAULTMUTED)
        m.template.player[m.eventConfig.METADEFAULTMUTED] = m.defaultMute
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEFAULTPLAYBACKRATE)
        m.template.player[m.eventConfig.METADEFAULTPLAYBACKRATE] = m.defaultRate
    end if
        If checkIfMetaConfigured(m.eventConfig.METAPLAYERHEIGHT)
        m.template.player[m.eventConfig.METAPLAYERHEIGHT] = m.playerHeight
    end if
          If checkIfMetaConfigured(m.eventConfig.METAPLAYERWIDTH)
        m.template.player[m.eventConfig.METAPLAYERWIDTH] = m.playerWidth
    end if
    If checkIfMetaConfigured(m.eventConfig.METATITLE)
        
        metatitle = ""
        if m.videoType = "Ad" and m.adTitle <> invalid
        metatitle = m.adTitle
        end if
        if m.player <> invalid
            if m.player.content.title <> invalid and m.videoType = "Content"
               metatitle = m.player.content.title
            end if
            if m.player.content.url <> invalid and m.videoType = "Content"
               metaurl = m.player.content.url
            end if
        end if
        m.template.video[m.eventConfig.METATITLE] = metatitle
    end if
    
    If checkIfMetaConfigured(m.eventConfig.METAFRAMERATE)
        if m.player.FrameRate <> invalid
        m.template.video[m.eventConfig.METAFRAMERATE] = m.player.FrameRate
        else 
        m.template.video[m.eventConfig.METAFRAMERATE] = 30
        end if
    end if
        
    End if
        
end function

' Function to get video URL/set url in base
function getContentUrl()
    if m.player <> invalid
       if m.player.content <> invalid
            if m.player.content.Url <> invalid
                setContentUrlToBase(m.player.content.Url)
                return m.player.content.Url
            else
                return ""
            end if
        else
            return ""
        end if
    end if
end function

     