sub init()
    m.top.functionName = "getContent"
    getGeoContent()
    getServerTime()
end sub

sub getContent()
    requestData = m.top.requestData
    resultObject = utils_HTTPRequest(requestData.httpMethodString, requestData.urlString, requestData.postBodyString, requestData.headersAssociativeArray)
    m.top.result = resultObject
end sub

sub getGeoContent()
    requestData = {}
    requestData.httpMethodString = "GET"
    url = "https://pro.ip-api.com/json/?key=ZP6KtPdtCCRcgGk"
    requestData.urlString = url
    requestData.postBodyString = invalid
    requestData.headersAssociativeArray = invalid
    resultObject = utils_HTTPRequest(requestData.httpMethodString, requestData.urlString, requestData.postBodyString, requestData.headersAssociativeArray)
    m.top.geoResult = resultObject
end sub

sub getServerTime()
    requestData = {}
    requestData.httpMethodString = "GET"
    url = "https://stagingbroker.datazoom.io/broker/v1/getEpochMillis"
    requestData.urlString = url
    requestData.postBodyString = invalid
    requestData.headersAssociativeArray = invalid
    resultObject = utils_HTTPRequest(requestData.httpMethodString, requestData.urlString, requestData.postBodyString, requestData.headersAssociativeArray)
    m.top.serverTimeResult = resultObject
end sub