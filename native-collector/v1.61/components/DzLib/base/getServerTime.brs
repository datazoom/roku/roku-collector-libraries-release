sub init()
    m.top.functionName = "getServerTime"
end sub

sub getServerTime()
    requestData = {}
    requestData.httpMethodString = "GET"
    url = "https://stagingbroker.datazoom.io/broker/v1/getEpochMillis"
    requestData.urlString = url
    requestData.postBodyString = invalid
    requestData.headersAssociativeArray = invalid
    resultObject = utils_HTTPRequest(requestData.httpMethodString, requestData.urlString, requestData.postBodyString, requestData.headersAssociativeArray)
    m.top.serverTimeResult = resultObject
end sub

